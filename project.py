from flask import Flask, render_template, request
from flask import redirect, jsonify, url_for
from flask import flash, session as login_session, make_response
import httplib2
import requests
import json
from sqlalchemy import create_engine, desc
from sqlalchemy.orm import sessionmaker
from database_setup import Base, Category, Item, User
from oauth2client.client import flow_from_clientsecrets
from oauth2client.client import FlowExchangeError
app = Flask(__name__)

#engine = create_engine(
#    'sqlite:///catalog.db',
#    connect_args={
#        'check_same_thread': False})
engine = create_engine('postgresql://catalog:root@localhost/catalog')

Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

# home page showing last data and categories


@app.route('/')
@app.route('/home/')
def index():
    categories = session.query(Category)
    items = session.query(Item).order_by(desc(Item.added_at)).limit(7)
    return render_template('home.html', categories=categories, items=items)

# add new category , this function only used to add categories but on
# production it will be deleted


@app.route('/home/addcategory', methods=['GET', 'POST'])
def newCategory():
    if 'username' not in login_session:
        return redirect(url_for('index'))
    elif request.method == 'POST':
        cat = Category(name=request.form['name'])
        session.add(cat)
        session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('newcategory.html')


# add new item
@app.route('/home/additem', methods=['GET', 'POST'])
def newItem():
    if 'username' not in login_session:
        return redirect(url_for('index'))
    if request.method == 'POST':
        item = Item(title=request.form['title'],
                    description=request.form['description'],
                    category_id=request.form['category_id'])
        session.add(item)
        session.commit()
        return redirect(url_for('index'))
    else:
        categories = session.query(Category)
        items = session.query(Item)
        return render_template(
            'newitem.html',
            categories=categories,
            items=items)

# delete an item


@app.route('/home/delete/<int:itemId>', methods=['POST'])
def deleteItem(itemId):
    item = session.query(Item).filter_by(id=itemId).one()
    if 'username' in login_session:
        session.delete(item)
        session.commit()
    return redirect(url_for('index'))

# edit an item


@app.route('/home/edit/<int:itemId>', methods=['POST', 'GET'])
def editItem(itemId):
    if 'username' not in login_session:
        return redirect(url_for('index'))
    item = session.query(Item).filter_by(id=itemId).one()
    categories = session.query(Category)
    if request.method == 'GET':
        return render_template(
            'editItem.html',
            item=item,
            categories=categories)
    else:
        if request.form['title']:
            item.title = request.form['title']
        if request.form['description']:
            item.description = request.form['description']
        if request.form['category_id']:
            item.category_id = request.form['category_id']

        session.add(item)
        session.commit()
        return redirect(url_for('showItem', item_id=item.id))

# this for showing specific category items


@app.route('/home/category/<int:cat_id>', methods=['GET'])
def showCategory(cat_id):
    items = session.query(Item).filter_by(category_id=cat_id)
    category = session.query(Category).filter_by(id=cat_id).one()
    categories = session.query(Category)
    count = items.count()
    return render_template(
        'home.html',
        categories=categories,
        items=items,
        category=category,
        count=count)

# this for showing specific item


@app.route('/home/item/<int:item_id>', methods=['GET'])
def showItem(item_id):
    item = session.query(Item).filter_by(id=item_id).one()
    category = session.query(Category).filter_by(id=item.category_id).one()
    return render_template('item.html', item=item, category=category)


CLIENT_ID = 'here is your client id'
CLIENT_SECRET = 'you client secret'
# this fuction is used to login with google account


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        # first check the state token
        if request.args.get('state') != login_session['state']:
            response = make_response(json.dumps('Invalid state.'), 401)
            response.headers['Content-Type'] = 'application/json'
            return response
        code = request.data
        # Upgrade the authorization code into a credentials object of throw failed
        try:
            oauth_flow = flow_from_clientsecrets(
                'client_secrets.json', scope='')
            oauth_flow.redirect_uri = 'home'
            credentials = oauth_flow.step2_exchange(code)
        except FlowExchangeError:
            response = make_response(json.dumps(
                'Failed to upgrade the authorization code.'), 401)
            response.headers['Content-Type'] = 'application/json'
            return response
        # Check that the access token is valid.
        access_token = credentials.access_token
        url = ('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s' %
               access_token)
        hreq = httplib2.Http()
        res = json.loads(hreq.request(url, 'GET')[1])
        # If there was an error in the access token info, abort.
        if res.get('error') is not None:

            response = make_response(json.dumps(res.get('error')), 500)
            response.headers['Content-Type'] = 'application/json'
            return response

        # Verify that the access token is used for the intended user.

        gplus_id = credentials.id_token['sub']

        if res['user_id'] != gplus_id:

            response = make_response(json.dumps(
                "Token's user ID doesn't match given user ID."), 401)
            response.headers['Content-Type'] = 'application/json'
            return response

        # Verify that the access token is valid for this app.

        if res['issued_to'] != CLIENT_ID:

            response = make_response(json.dumps(
                "Token's client ID does not match app's."), 401)
            response.headers['Content-Type'] = 'application/json'
            return response

        stored_access_token = login_session.get('access_token')

        stored_gplus_id = login_session.get('gplus_id')

        if stored_access_token is not None and gplus_id == stored_gplus_id:

            response = make_response(json.dumps(
                'Current user is already connected.'), 200)
            response.headers['Content-Type'] = 'application/json'
            return response

        # Store the access token in the session for later use.

        login_session['access_token'] = credentials.access_token

        login_session['gplus_id'] = gplus_id

        # Get user data

        userinfo_url = "https://www.googleapis.com/oauth2/v1/userinfo"

        params = {'access_token': credentials.access_token, 'alt': 'json'}

        answer = requests.get(userinfo_url, params=params)

        # converting data to json

        data = answer.json()
        login_session['username'] = data['name']

        login_session['email'] = data['email']

        login_session['provider'] = 'google'

        # try to get the user if exist
        try:
            user = session.query(User).filter_by(email=data["email"]).one()
            user_id = user.id

        except BaseException:
            user_id = -1  # user not exist
        # if not exist create one

        if user_id == -1:
            newUser = User(
                name=login_session['username'], email=login_session['email'])
            session.add(newUser)
            session.commit()
            user = session.query(User).filter_by(
                email=login_session['email']).one()
            user_id = user.id

        login_session['user_id'] = user_id

        items = session.query(Item)
        categories = session.query(Category)
        return render_template(
            'home.html',
            categories=categories,
            items=items,
            user=login_session['username'])


@app.route('/logout')
def logout():
    # Only disconnect a connected user.
    access_token = login_session.get('access_token')
    if access_token is None:
        response = make_response(json.dumps(
            'Current user is not connected yet.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    url = 'https://accounts.google.com/o/oauth2/revoke?token=%s' % access_token
    hreq = httplib2.Http()
    res = hreq.request(url, 'GET')[0]
    if res['status'] == '200':
        response = make_response(json.dumps(
            'Successfully disconnected hope to see you later.'), 200)
        response.headers['Content-Type'] = 'application/json'
        return response
    else:
        response = make_response(json.dumps(
            'Failed to revoke token for given user.', 400))
        response.headers['Content-Type'] = 'application/json'
        return response

# this API will return the data of the catalog


@app.route('/catalog.json')
def catalogInfo():
    cats = session.query(Category)
    category = {}
    category['Category'] = []

    for cat in cats:
        temp = {'id': cat.id, 'name': cat.name, 'items': []}
        items = session.query(Item).filter_by(category_id=cat.id).all()
        for item in items:
            tempItem = {'id': item.id, 'tite': item.title,
                        'description': item.description}
            temp['items'].append(tempItem)
        category['Category'].append(temp)
    return jsonify(category)


if __name__ == '__main__':
    app.secret_key = 'super_secret_key'
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
